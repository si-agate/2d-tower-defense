﻿using UnityEngine;



public class Enemy : MonoBehaviour

{

    [SerializeField] private int _maxHealth = 1;

    [SerializeField] private float _moveSpeed = 1f;

    [SerializeField] private SpriteRenderer _healthBar;

    [SerializeField] private SpriteRenderer _healthFill;

    [SerializeField] private int _maxShield = 1;

    [SerializeField] private SpriteRenderer _shieldBar;

    [SerializeField] private SpriteRenderer _shieldFill;

    [SerializeField] private SpriteRenderer _shieldIcon;



    private int _currentHealth;
    private int _currentShield;
    private float healthPercentage;
    private float shieldPercentage;
    public Vector3 TargetPosition { get; private set; }
    public int CurrentPathIndex { get; private set; }
    bool isShieldOn = true;

    private void Start()
    {
        if(_maxShield == 0)
        {
            _healthFill.size = _healthBar.size;
        }
    }

    // Fungsi ini terpanggil sekali setiap kali menghidupkan game object yang memiliki script ini
    private void OnEnable()
    {
        if (_maxShield != 0)
        {
            _currentShield = _maxShield;
            _currentHealth = _maxHealth;
            _healthFill.size = _healthBar.size;
            _shieldFill.size = _shieldBar.size;
        }

        else
        {
            _currentShield = _maxShield;
            _currentHealth = _maxHealth;
            _shieldBar.size = new Vector2(0, 0);
            _healthFill.size = _healthBar.size;
            _shieldFill.size = _shieldBar.size;
            _shieldIcon.enabled = false;
            isShieldOn = false;
        }




    }

    public void MoveToTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, TargetPosition, _moveSpeed * Time.deltaTime);
    }

    public void SetTargetPosition(Vector3 targetPosition)
    {
        TargetPosition = targetPosition;
        _healthBar.transform.parent = null;
        _shieldBar.transform.parent = null;

        // Mengubah rotasi dari enemy
        Vector3 distance = TargetPosition - transform.position;

        if (Mathf.Abs(distance.y) > Mathf.Abs(distance.x))
        {
            // Menghadap atas
            if (distance.y > 0)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
            }
            // Menghadap bawah
            else
            {
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, -90f));
            }
        }
        else
        {
            // Menghadap kanan (default)
            if (distance.x > 0)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            }
            // Menghadap kiri
            else
            {
                transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 180f));
            }
        }

        _healthBar.transform.parent = transform;
        _shieldBar.transform.parent = transform;
    }

    // Menandai indeks terakhir pada path
    public void SetCurrentPathIndex(int currentIndex)
    {
        CurrentPathIndex = currentIndex;
    }

    public void ReduceEnemyHealth(int damage)
    {
        if ((_currentShield > 0) && isShieldOn)
        {
            _currentShield -= damage;
            AudioPlayer.Instance.PlaySFX("hit-enemy");
            shieldPercentage = (float)_currentShield / _maxShield;
        }

        else if ((_currentShield <= 0) && isShieldOn)

        {
            _shieldIcon.enabled = false;
            _currentShield = 0;
            AudioPlayer.Instance.PlaySFX("hit-enemy");
            _shieldBar.enabled = false;
            _shieldFill.enabled = false;
            isShieldOn = false;
            shieldPercentage = (float)_currentShield / _maxShield;
            healthPercentage = (float)_currentHealth / _maxHealth;
        }

        else if ((_currentHealth > 0) && !isShieldOn)
        {
            _currentShield = 0;
            _currentHealth -= damage;
            AudioPlayer.Instance.PlaySFX("hit-enemy");
            healthPercentage = (float)_currentHealth / _maxHealth;
        }

        else if ((_currentHealth <= 0) && !isShieldOn)

        {
            _currentShield = 0;
            _currentHealth = 0;
            healthPercentage = (float)_currentHealth / _maxHealth;
            gameObject.SetActive(false);
            AudioPlayer.Instance.PlaySFX("enemy-die");

        }

        if (healthPercentage <= 0) healthPercentage = 0;

        _healthFill.size = new Vector2(healthPercentage * _healthBar.size.x, _healthBar.size.y);


        if (shieldPercentage <= 0) shieldPercentage = 0;

        _shieldFill.size = new Vector2(shieldPercentage * _shieldBar.size.x, _shieldBar.size.y);

    }

}
